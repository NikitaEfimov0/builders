# Builders


Репозиторий 6 модуля марафона по Android-разработке.
Задания:
1) [Function literals with reciever](https://gitlab.com/NikitaEfimov0/builders/-/blob/main/Function_literals_with_reciever.kt)
2) [String and map builders](https://gitlab.com/NikitaEfimov0/builders/-/blob/main/string_and_map_builders.kt)
3) [The function apply](https://gitlab.com/NikitaEfimov0/builders/-/blob/main/the_function_apply.kt)
4) [HTML builder](https://gitlab.com/NikitaEfimov0/builders/-/blob/main/html_builder.kt)
5) [Builders how it works](https://gitlab.com/NikitaEfimov0/builders/-/blob/main/Bulders_how_it_works.kt)
6) [Builders implementation](https://gitlab.com/NikitaEfimov0/builders/-/blob/main/builders_implementation.kt)

[Скриншот успешной сдачи тестов](https://gitlab.com/NikitaEfimov0/builders/-/blob/main/Снимок_экрана_2022-03-12_в_01.12.38.png)
