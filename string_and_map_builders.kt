import java.util.HashMap

fun <I, S> buildMutableMap(build: HashMap<I, S>.() -> Unit): Map<I, S> {
    val map = HashMap<I, S>()
    map.build()
    return map
}

fun usage(): Map<Int, String> {
    return buildMutableMap {
        put(0, "0")
        for (i in 1..10) {
            put(i, "$i")
        }
    }
}
