fun task(): List<Boolean> {
    val isEven: Int.() -> Boolean = { if(this%2==0)true else false }
    val isOdd: Int.() -> Boolean = { if(this%2!=0)true else false }

    return listOf(42.isOdd(), 239.isOdd(), 294823098.isEven())
}
